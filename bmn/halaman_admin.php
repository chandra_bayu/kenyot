<!DOCTYPE html>
<html>
<head>
	<title>SIMFOMIS BMN</title>
    <meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SIMFOMIS BMN</title>
    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/fullcalendar.min.css"/>
	<link href="asset/css/style.css" rel="stylesheet">
	<link rel="shortcut icon" href="asset/img/logo.jpg">
	<script>
	function deleletconfig(){

	var del=confirm("Are you sure you want to delete this record?");
	if (del==true){
    alert ("record deleted")
	}
	return del;
	}
	</script>
</head>

<body id="mimin" class="dashboard">
	<?php 
	session_start();
 
	// cek apakah yang mengakses halaman ini sudah login
	if($_SESSION['level']==""){
		header("location:index.php?pesan=gagal");
	}
 
	?>
	
	<nav class="navbar navbar-default header navbar-fixed-top">
        <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <a href="halaman_pegawai.html" class="navbar-brand"> 
                 <b>ADMIN</b>
              </a>

              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span>Anda Login Sebagai : <?php echo $_SESSION['nama']; ?>!</span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="asset/img/logo.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                      <ul>
                        <li><a href="logout.php"><span class="fa fa-power-off "> Logout</span></a></li>
                      </ul>
                  </ul>
                </li>
              </ul>
            </div>
        </div>
    </nav>
	
	<div class="container-fluid mimin-wrapper">
		<div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
						  <h1 class="animated fadeInLeft">21:00</h1>
						  <p class="animated fadeInRight">Sat,October 1st 2029</p>
					</li>
						<li class="active ripple">
							<a class="tree-toggle nav-header"><span class="fa-home fa"></span> Home
								<span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
							<ul class="nav nav-list tree">
							  <li><a href="halaman_admin.php">Admin</a></li>
							  <li><a href="add_user.php">Tambah User</a></li>
							</ul>
						</li>
						<li class="ripple">
							<a class="tree-toggle nav-header"><span class="fa fa-pencil-square"></span> Task
								<span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
							<ul class="nav nav-list tree">
							  <li><a href="add_tugas.php">Tambah Tugas</a></li>
							  <li><a href="upload.php">Upload File</a></li>
							</ul>
						</li>
						<li class="ripple">
							<a href="download.php"><span class="fa fa-check-square-o"> View </span></a>
						</li>
                </ul>
              </div>
        </div>
		
		<div id="content">
                <div class="panel">
                  <div class="panel-body">
                      <div class="col-md-9 col-sm-12">
                        <h3 class="animated fadeInLeft">Dokumentasi Administrasi BMN BPS Provinsi Jawa Barat</h3>
                      </div>
                  </div>                    
                </div>

                <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>User Tables</h3></div>
						<div class="panel-body">
							<div class="responsive-table">
								<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
									<thead align="center">
										<tr>
											<th>SATKER</th>
											<th>KODE SATKER</th>
											<th>LEVEL</th>
											<th>ACTION</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$konek = mysqli_connect("localhost","root","","bmn");
									$query = "SELECT * FROM user ORDER BY id DESC";
									$hasil = mysqli_query($konek, $query);
									
									while ($r = mysqli_fetch_array($hasil))
									{
									echo "<tr>
											<td>$r[nama]</td>
											<td>$r[username]</td>
											<td>$r[level]</td>
											<td><a href='delete_user.php?id_user=$r[id]' onClick=\"javascript:return confirm('are you sure you want to delete this?');\">Delete</a></td>
										  </tr>";
									}									
									?>
									</tbody>		
								</table>
							</div>
						</div> 
				  </div>
				</div>
			</div>
      	</div>
		
	</div>
	
	<!--
	<div class="container">
    	<div class="header">
    		<h1>Dokumentasi Administrasi BMN</h1>
        </div>
		
		<div class="menu">
			<div class="dropdown">
				Anda Login Sebagai : <?php echo $_SESSION['nama']; ?>!<span class="lambang-panah"></span>
				<div class="dropdown-content">
					<a href="logout.php">LOGOUT</a>
				</div>
			</div>
		</div>
		
        <div class="section">
			<div class="nav">
				<div class="dropdown">
						<button class="dropbtn"><li><a href="halaman_admin.php" class="active">Admin</a></li></button>
						<div class="dropdown-content">
						<a href="add_user.php" >Tambah User</a>
						</div>
				</div></br>
				<div class="dropdown">
						<button class="dropbtn"><li><a href="halaman_home_admin.php" >Home</a></li></button>
				</div></br>
				<div class="dropdown">
						<button class="dropbtn"><li><a href="upload.php">Upload</a></li></button>
							<div class="dropdown-content">
							<a href="add_tugas.php" >Tambah Tugas</a>
							</div>
				</div></br>
				<div class="dropdown">
						<button class="dropbtn"><li><a href="download.php">Download</a></li></button>
				</div>
			</div>
 
			<div class="article">
        	<h2>Admin</h2></br></br>
            
			
			<?php
			$konek = mysqli_connect("localhost","root","","bmn");
			$query = "SELECT * FROM user ORDER BY id DESC";
			$hasil = mysqli_query($konek, $query);
			
			echo '<table width = 500 border =1>
					<thead>
						<tr>
							<th>SATKER</th>
							<th>KODE SATKER</th>
							<th>LEVEL</th>
							<th>ACTION</th>
						</tr>
					</thead>
				<tbody>';
			
			while ($r = mysqli_fetch_array($hasil))
			{
			echo "<tr>
					<td>$r[nama]</td>
					<td>$r[username]</td>
					<td>$r[level]</td>
					<td><a href='delete_user.php?id_user=$r[id]' onClick=\"javascript:return confirm('are you sure you want to delete this?');\">Delete</a></td>
				  </tr>";
			}
			
			echo'</tbody>
				</table>';
			
			?>
			
			</div>
			<div class="clear"></div>
			
		</div>
		
		<div class="clear"></div>
			<div class="footer">
			copyright&copy BPS Provinsi Jawa Barat
			</div>
		
    </div>
	-->
	
	<!-- start: Javascript -->
<script src="asset/js/jquery.min.js"></script>
<script src="asset/js/jquery.ui.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="asset/js/plugins/moment.min.js"></script>
<script src="asset/js/plugins/jquery.datatables.min.js"></script>
<script src="asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="asset/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="asset/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
</body>

</html>