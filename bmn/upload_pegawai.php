<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SIMFOMIS BMN</title>
    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="asset/css/plugins/datatables.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
  <link href="asset/css/style.css" rel="stylesheet">
  <link rel="shortcut icon" href="asset/img/logo.jpg">
</head>

<body id="mimin" class="dashboard">
	<?php 
	session_start();
 
	// cek apakah yang mengakses halaman ini sudah login
	if($_SESSION['level']==""){
		header("location:index.php?pesan=gagal");
	}
	$login = $_SESSION['nama'];
	?>
	
	<nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              
			  <a href="upload_pegawai.php" class="navbar-brand"> 
                 <b>UPLOAD</b>
                </a>

              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span>Anda Login Sebagai : <?php echo $_SESSION['nama']; ?>!</span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="asset/img/logo.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <ul>
                        <li><a href="logout.php"><span class="fa fa-power-off "> Logout</span></a></li>
                      </ul>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
    </nav>
	
	<div class="container-fluid mimin-wrapper">
		<div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
						  <h1 class="animated fadeInLeft">21:00</h1>
						  <p class="animated fadeInRight">Sat,October 1st 2029</p>
					</li>
                    <li class="ripple">
						<a href="halaman_pegawai.php"><span class="fa-home fa"> Home </span></a>
                    </li>
                    <li class="active ripple">
						<a href="upload_pegawai.php"><span class="fa fa-pencil-square"> Upload </span></a>
                    </li>
                    <li class="ripple">
						<a href="download_pegawai.php"><span class="fa fa-check-square-o"> View </span></a>
                    </li>
                </ul>
              </div>
        </div>
		
		<div id="content">
            <div class="panel">
                <div class="panel-body">
                    <div class="col-md-9 col-sm-12">
                        <h3 class="animated fadeInLeft">Dokumentasi Administrasi BMN BPS Provinsi Jawa Barat</h3>
                    </div>
                </div>                    
            </div>

            <div class="col-md-12 top-20 padding-0">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading"><h3>Data Tables</h3></div>
							<div class="panel-body">
								<div class="responsive-table">
                                    <p>Upload file Anda dengan melengkapi form di bawah ini.</p>
										<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
											<thead align="center">
												<tr>
													<th>NAMA TUGAS</th>
													<th>DESKRIPSI</th>
													<th>PILIH FILE</th>
													<th>ACTION</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$id_posting = $_SESSION['username'];
												$konek = mysqli_connect("localhost","root","","bmn");
												$query = "SELECT * FROM tugas WHERE user = '$id_posting' and hit!='1' ORDER BY user ASC";
												$hasil = mysqli_query($konek, $query);
												
												while ($r = mysqli_fetch_array($hasil))
												{ 
													$no = 1;
													?>
												<tr>
												<form enctype="multipart/form-data" method="POST" action="proses.php">
													<td>
														<input type="hidden" name="id" value="<?php echo $r['id']; ?>">
														<input type="hidden" name="nama_tugas" value="<?php echo $r['nama_tugas']; ?>">
														<?php echo $r['nama_tugas']; ?>
													</td>
													<td>
														<input type="text" name="name" rows="1" cols="10">
													</td>
													<td>
														<input type="file" name="fupload">
													</td>
													<td>
														<input type="submit" name="simpan" value="SIMPAN">
													</td>
												<?php
												}
												?>
												</form>
												</tr>
												
											</tbody>
										</table>
								</div>
                            </div> 
                    </div>
                </div>
            </div>
        </div>
	</div>
	
	<!-- start: Javascript -->
<script src="asset/js/jquery.min.js"></script>
<script src="asset/js/jquery.ui.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="asset/js/plugins/moment.min.js"></script>
<script src="asset/js/plugins/jquery.datatables.min.js"></script>
<script src="asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="asset/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="asset/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
    
</body>
</html>