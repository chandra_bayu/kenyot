<!DOCTYPE html>
<html>
<head>
	<title>SIMFOMIS BMN</title>
    <meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SIMFOMIS BMN</title>
    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
      <link rel="stylesheet" type="text/css" href="asset/css/plugins/fullcalendar.min.css"/>
	<link href="asset/css/style.css" rel="stylesheet">
	<link rel="shortcut icon" href="asset/img/logo.jpg">
</head>
<body id="mimin" class="dashboard">
	<?php 
	session_start();
 
	// cek apakah yang mengakses halaman ini sudah login
	if($_SESSION['level']==""){
		header("location:index.php?pesan=gagal");
	}
 
	?>
	
	<nav class="navbar navbar-default header navbar-fixed-top">
        <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <a href="halaman_pegawai.html" class="navbar-brand"> 
                 <b>TASK</b>
              </a>

              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span>Anda Login Sebagai : <?php echo $_SESSION['nama']; ?>!</span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="asset/img/logo.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                      <ul>
                        <li><a href="logout.php"><span class="fa fa-power-off "> Logout</span></a></li>
                      </ul>
                  </ul>
                </li>
              </ul>
            </div>
        </div>
    </nav>
	<div class="container-fluid mimin-wrapper">
		<div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="time">
						  <h1 class="animated fadeInLeft">21:00</h1>
						  <p class="animated fadeInRight">Sat,October 1st 2029</p>
					</li>
						<li class="active ripple">
							<a class="tree-toggle nav-header"><span class="fa-home fa"></span> Home
								<span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
							<ul class="nav nav-list tree">
							  <li><a href="halaman_admin.php">Admin</a></li>
							  <li><a href="add_user.php">Tambah User</a></li>
							</ul>
						</li>
						<li class="ripple">
							<a class="tree-toggle nav-header"><span class="fa fa-pencil-square"></span> Task
								<span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
							<ul class="nav nav-list tree">
							  <li><a href="add_tugas.php">Tambah Tugas</a></li>
							  <li><a href="upload.php">Upload File</a></li>
							</ul>
						</li>
						<li class="ripple">
							<a href="download.php"><span class="fa fa-check-square-o"> View </span></a>
						</li>
                </ul>
              </div>
        </div>
		
		<div id="content">
                <div class="panel">
                  <div class="panel-body">
                      <div class="col-md-9 col-sm-12">
                        <h3 class="animated fadeInLeft">Dokumentasi Administrasi BMN BPS Provinsi Jawa Barat</h3>
                      </div>
                  </div>                    
                </div>

                <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Form Tambah Tugas</h3></div>
						<div class="panel-body">
							<div class="responsive-table">
								<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
									<?php
									$konek = mysqli_connect("localhost","root","","bmn");
									$query = "SELECT * FROM user ORDER BY id ASC";
									$hasil = mysqli_query($konek, $query);
									?>
										
									<form method="post" action="">
									<tr><th><label>Input Nama Tugas	:</label></th><th><input type="text" name="namatugas"></th></tr>
									
									<tr>
										<th>Tugas diberikan kepada :</th>
										<th>Pilih Satker</th>
									</tr>
								<tr>
									<td></td>
									<td><input type="checkbox" name="allbox" value="check" onclick="checkAll(0);" />Check All</td>
								</tr>
								<?php
								while ($r = mysqli_fetch_array($hasil))
								{
								?>
										
										<tr>
											<td></td>
											<td><input type="checkbox" name="user[]" value="<?php echo "$r[username]"?>"><?php echo $r['username']?></td>
											<td><?php echo "$r[nama]"?></td>
										</tr>
								<?php
								}
								
									if(isset($_POST["submit"])) 
									{
									$jumlah = count($_POST['user']); //menghitung jumlah value yang di centang
									for($i=0; $i<$jumlah; $i++){
									
									$foldtugas = $_POST['namatugas'];
									$fold = $_POST['user'][$i]; 
									$nama_folder = "$fold/$foldtugas"; 
									
									 //mengecek keberadaan folder
									 if((file_exists($nama_folder))&&(is_dir($nama_folder))) 
									 { 
									 echo "Folder <b>".$nama_folder."</b> Sudah ada"; 
									 } 
									 else 
									 { 
									 //memasukan fungsi mkdir 
									 $fd = mkdir ($nama_folder);
									$sql = "insert into tugas ( nama_tugas, user )" . 
									"values ( '$foldtugas', '$fold' )";				 
									mysqli_query($konek, $sql);
									//untuk pengecekan proses 
									 if ($fd) { 
									 echo "<script>alert('Data berhasil di tambahkan!');window.location='add_tugas.php';</script>"; 
									 } 
									 else { 
									 echo "<script>alert('Semua data diperlukan. Harap isi semua.!');window.location='add_tugas.php';</script>";
									 }
									}
									}
									}					
									?>
									</table>
									<input type="submit" name="submit" value="Input">
										<a href="halaman_home_admin.php">Kembali</a>
								</form>			
								
							</div>
						</div> 
				  </div>
				</div>
			</div>
      	</div>
		
	</div>
	
	<!-- start: Javascript -->
    <script type="text/javascript" src="asset/js/jquery.min.js"></script>
    <script type="text/javascript" src="asset/js/jquery.ui.min.js"></script>
    <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
	
	<!-- plugins -->
    <script src="asset/js/plugins/moment.min.js"></script>
    <script src="asset/js/plugins/fullcalendar.min.js"></script>
    <script src="asset/js/plugins/jquery.nicescroll.js"></script>
    <script src="asset/js/plugins/jquery.vmap.min.js"></script>
    <script src="asset/js/plugins/maps/jquery.vmap.world.js"></script>
    <script src="asset/js/plugins/jquery.vmap.sampledata.js"></script>
    <script src="asset/js/plugins/chart.min.js"></script>
	
	<!-- custom -->
     <script src="asset/js/main.js"></script>
	<script type="text/javascript"> 
	//check all checkbox
	function checkAll(form){
		for (var i=0;i<document.forms[form].elements.length;i++)
		{
			var e=document.forms[form].elements[i];
			if ((e.name !='allbox') && (e.type=='checkbox'))
			{
				e.checked=document.forms[form].allbox.checked;
			}
		}
	}
	</script>
</body>

</html>